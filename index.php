<?php include_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>S04: Access Modifiers and Encapsulation</title>
  <link rel="stylesheet" href="./style.php" media="screen">
</head>

<body>
  <div class="container">
    <h1>Building</h1>
    <?php
    $buildingName = $building->getName();
    $buildingFloors = $building->getFloors();
    $buildingAddress = $building->getAddress();
    echo "<p>The name of the building is $buildingName.</p>";
    echo "<p>The $buildingName has $buildingFloors floors.</p>";
    echo "<p>The $buildingName is located at $buildingAddress.</p>";
    $building->setName('Caswynn Complex');
    $buildingName = $building->getName();
    echo "<p>The name of the building has been changed to $buildingName.<p>";
    ?>
  </div>

  <div class="container">
    <h1>Condominium</h1>
    <?php
    $condominiumName = $condominium->getName();
    $condominiumFloors = $condominium->getFloors();
    $condominiumAddress = $condominium->getAddress();
    echo "<p>The name of the condominium is $condominiumName.</p>";
    echo "<p>The $condominiumName has $condominiumFloors floors.</p>";
    echo "<p>The $condominiumName is located at $condominiumAddress.</p>";
    $condominium->setName('Enzo Tower');
    $condominiumName = $condominium->getName();
    echo "<p>The name of the condominium has been changed to $condominiumName.<p>";
    ?>
  </div>
</body>

</html>